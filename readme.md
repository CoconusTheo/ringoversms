# Ringover SMS

Petit projet pour envoyer des SMS avec l'API de Ringover

## Déploiement
La branche 'master' de ce repository GIT est déployé automatiquement sur l'hébergeur statique AWS Amplify

Pendant le déploiement, les commandes `npm run install` et `npm run build` vont être exécutées automatiquement.

Ensuite le contenu du dossier `dist` va être accessible a l'addresse [master.d20kpbuao8kmp4.amplifyapp.com](https://master.d20kpbuao8kmp4.amplifyapp.com/)

## Contribution
Pour pouvoir contribuer il suffit de faire une pull-request sur bitbucket.

Un aperçu de la pull-request sera accessible à l'adresse 

`pr-[NUMERO_DE_PR].d20kpbuao8kmp4.amplifyapp.com`.
